package com.example.demo;

import org.springframework.stereotype.Service;

// enum for state machine
enum CalculatorState {
    INIT,
    FIRST_NUMBER,
    SECOND_NUMBER,
    RESULT,
    ERROR
}

@Service
public class CalculatorService {

    CalculatorState currentState = CalculatorState.INIT;
    double firstNumber = 0.0;
    double secondNumber = 0.0;
    String operator = "";
    double result = 0.0;
    String resultStr = "";

    public double calculate(double num1, double num2, String op) {
        switch (op) {
            case "+":
                return num1 + num2;
            case "-":
                return num1 - num2;
            case "*":
                return num1 * num2;
            case "/":
            if (num2 == 0 && num1 == 0) {
                throw new IllegalArgumentException("Invalid input: " + num1 + op + num2);
            }
                return num1 / num2;
            default:
                throw new IllegalArgumentException("Invalid operator: " + op);
        }
    }

    public String eval(String input) throws IllegalArgumentException {
        // get each charater from input
        // if character is a number, add to number string
        // if character is an operator, add to operator string

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (Character.isDigit(c)) {
                // cast to integer
                int num = Character.getNumericValue(c);
                this.processNumber(num);
            }
            else {
                this.processSymbol(c);
            }
        }

        return this.resultStr;
    }
    
    private void processNumber(int num) {
        switch (this.currentState) {
            case INIT:
            this.firstNumber = num;
            this.currentState = CalculatorState.FIRST_NUMBER;
            this.resultStr = String.valueOf(num);
                break;
            case FIRST_NUMBER:
                this.firstNumber = this.firstNumber * 10 + num;
                this.resultStr = String.valueOf(this.firstNumber);
                break;
            case SECOND_NUMBER:
                this.secondNumber = this.secondNumber * 10 + num;
                this.resultStr = this.resultStr + String.valueOf(num);
                break;
            case RESULT:
                break;
        }
    }

    private void processSymbol(char c) throws IllegalArgumentException {
        switch (this.currentState) {
            case INIT:
                throw new IllegalArgumentException("Invalid input");
            case FIRST_NUMBER:
            if(c == '+' || c == '-' || c == '*' || c == '/') {
                this.operator = String.valueOf(c);
                this.currentState = CalculatorState.SECOND_NUMBER;
                this.resultStr = this.resultStr + this.operator;
            }else {
                throw new IllegalArgumentException("Invalid input");
            }
                break;
            case SECOND_NUMBER:
                if(c == '=') {
                    this.result = this.calculate(this.firstNumber, this.secondNumber, this.operator);
                    this.resultStr = this.resultStr + "=" +  String.valueOf(this.result);
                    this.clearCalculator();

                }else {
                    throw new IllegalArgumentException("Invalid input");
                }
                break;
            case RESULT:
                break;
        }
    }

    private void clearCalculator() {
        this.currentState = CalculatorState.INIT;
        this.firstNumber = 0.0;
        this.secondNumber = 0.0;
        this.operator = "";
        this.result = 0.0;
    }
    
}
