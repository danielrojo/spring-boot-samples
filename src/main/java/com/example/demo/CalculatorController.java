package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculatorController {
 
    @Autowired
    CalculatorService calculatorService;

 
    @GetMapping("/calculator")
    public ResponseEntity<String> greeting(
            @RequestParam(value = "num1", defaultValue = "0.0") double num1,
            @RequestParam(value = "num2", defaultValue = "0.0") double num2,
            @RequestParam(value = "op", defaultValue = "add") String op) {
        {
            return new ResponseEntity<String>(
                    String.format("%f %s %f = %f", num1, op, num2,
                            this.calculatorService.calculate(num1, num2, op)),
                    org.springframework.http.HttpStatus.OK);
        }
    }

    @GetMapping("/eval")
    public ResponseEntity<String> eval(
            @RequestParam(value = "expr", defaultValue = "1*2=") String expr) {
        // return String.format(expr);
        try {
            return new ResponseEntity<String>(
                    String.format("%s = %s", expr,
                            this.calculatorService.eval(expr)),
                    org.springframework.http.HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<String>(
                    String.format("%s = %s", expr, e.getMessage()),
                    org.springframework.http.HttpStatus.BAD_REQUEST);
        }
    }


}
