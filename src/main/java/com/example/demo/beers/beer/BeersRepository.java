package com.example.demo.beers.beer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BeersRepository extends JpaRepository<BeerEntity, Long> {
    BeerEntity findById(long id);
    BeerEntity findByName(String beerName);
    List<BeerEntity> findByAbvBetween(double less, double greater);

}
