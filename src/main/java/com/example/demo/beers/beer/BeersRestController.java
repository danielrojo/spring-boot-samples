package com.example.demo.beers.beer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import jakarta.validation.Valid;
// ControllerAdvice
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;

import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;

@RestController
@RequestMapping("/api/v1")
public class BeersRestController {
    
    @Autowired
    private BeersService beersService;
    
    @GetMapping("/beers")
    public ResponseEntity<Object> getAllBeers(
    @RequestParam(name = "abv_gt", required = false, defaultValue = "0") String abvGt,
    @RequestParam(name = "abv_lt", required = false, defaultValue = "100") String abvLt) {
        // cast to double
        double abvGtDouble = Double.parseDouble(abvGt);
        double abvLtDouble = Double.parseDouble(abvLt);
        if(abvGtDouble > abvLtDouble || abvGtDouble < 0 || abvLtDouble > 100) {
            return ResponseEntity.badRequest().body(new ErrorResponse("Invalid parameters", 400));
        }
        
        return ResponseEntity.ok(this.beersService.getAllBeers(abvGtDouble, abvLtDouble));
    }

    
    @PostMapping("/beers")
    @Transactional
    public ResponseEntity<Object> createBeer(@RequestBody @Valid BeerDto beer) {
        BeerDto myBeer = this.beersService.getBeerByBeerName(beer.getName());
        if (myBeer != null) {
            return ResponseEntity.badRequest().body("Beer already exists");
        } else {
            return ResponseEntity.ok(this.beersService.addBeer(beer));
        }
    }

    
    @PutMapping("/beers/{id}")
    @Transactional
    public ResponseEntity<BeerDto> updateBeerById(@PathVariable long id, @RequestBody BeerDto beer) {
        BeerDto beerDto = this.beersService.getBeerById(id);
        if (beerDto != null) {
            return ResponseEntity.ok(this.beersService.updateBeer(beer, id));
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    
    
    @DeleteMapping("/beers/{id}")
    @Transactional
    public ResponseEntity<BeerDto> deleteBeer(@PathVariable long id) {
        BeerDto beerDto = this.beersService.getBeerById(id);
        if (beerDto != null) {
            return ResponseEntity.ok(this.beersService.deleteBeer(id));
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    

}


