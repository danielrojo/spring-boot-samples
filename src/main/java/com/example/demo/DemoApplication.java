package com.example.demo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.web.client.RestTemplate;

import com.example.demo.beers.beer.BeerEntity;
import com.example.demo.beers.beer.BeersRepository;
import com.example.demo.beers.beer.BeersService;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		// run command line runner
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Profile("create-beers")
	@Bean
	public CommandLineRunner addBeers(BeersRepository beerRepository, BeersService beersService) {
		return (args) -> {
			for (BeerEntity b : beersService.getBeersFromApi("https://api.punkapi.com/v2/beers")) {
				beerRepository.save(b);
			}

		};
	}

}
