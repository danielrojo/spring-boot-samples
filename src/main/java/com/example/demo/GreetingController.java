package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.library.users.UserDto;

import rx.Observable;

@Controller
public class GreetingController {

    @Autowired
    GreetingService greetingService;

	String result = "";

	@GetMapping("/greeting")
	public String greeting(Model model) {

		String[] heroes = {"Superman", "Batman", "Spiderman"};

		model.addAttribute("message", greetingService.greet());
		model.addAttribute("heroes", heroes);
		return "greeting";
	}

	@RequestMapping(value = "/saveStudent", method = RequestMethod.POST)
    public String saveStudent(@ModelAttribute UserDto student, BindingResult errors, Model model) {
        if (errors.hasErrors()) {
			model.addAttribute("message", errors);
			return "greeting";
		}
		model.addAttribute("student", student);
		return "greeting";
    }


}
