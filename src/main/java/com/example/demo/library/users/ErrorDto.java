package com.example.demo.library.users;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class ErrorDto implements ResponseDto {
    private int code;
    private String message;

}
