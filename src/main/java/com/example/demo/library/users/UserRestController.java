package com.example.demo.library.users;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;

@RestController
@RequestMapping("/api/v1")
@Transactional
public class UserRestController {

    @Autowired
    private UserService userService;

    @GetMapping("/users")
    public Iterable<UserDto> getUser() {
        return userService.getUsers();
    }

    @PostMapping("/users")
    public ResponseEntity<ResponseDto> addUser(@RequestBody UserDto user) {

        try {
            UserDto response = userService.addUser(user);
            return ResponseEntity.status(HttpServletResponse.SC_CREATED).body(response);
        } catch (Exception e) {
            return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST)
                    .body(new ErrorDto(400, "El DNI no puede ser modificado"));
        }

    }

    @PutMapping("/users/{id}")
    public ResponseEntity<ResponseDto> updateUser(@RequestBody UserDto user, @PathVariable long id) {
        UserDto myUser = userService.findById(id);
        if (myUser != null) {
            if (!myUser.getDniStr().equals(user.getDniStr())) {
                return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST)
                        .body(new ErrorDto(400, "El DNI no puede ser modificado"));
            }
            return ResponseEntity.ok(userService.updateUser(user, id));
        } else {
            return ResponseEntity.status(HttpServletResponse.SC_NOT_FOUND).body(
                    ErrorDto.builder()
                            .message("Usuario no encontrado")
                            .code(404)
                            .build());
        }
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<UserDto> deleteById(@PathVariable long id) {
        UserDto myUser = userService.findById(id);
        if (myUser != null) {
            userService.deleteById(id);
            return ResponseEntity.ok(myUser);
        } else {
            throw new NotFoundException("Usuario no encontrado");
        }
    }

}

