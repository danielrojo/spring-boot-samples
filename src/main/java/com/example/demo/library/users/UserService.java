package com.example.demo.library.users;

import java.util.ArrayList;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    
    @Autowired
    UserRepository repository;

    public Iterable<UserDto> getUsers() {
        Iterable<UserEntity> user = repository.findAll();
        Iterable<UserDto> response = new ArrayList<>();
        for(UserEntity u : user) {
            UserDto userDto = UserDto.builder()
                .id(u.getId())
                .firstName(u.getFirstName())
                .lastName(u.getLastName())
                .age(u.getAge())
                .dniStr(u.getDniStr())
                .email(u.getEmail())
                .build();
            ((ArrayList<UserDto>) response).add(userDto);
        }
        return response;
    }

    public UserDto getUserById(long id) {
        UserEntity user = repository.findById(id);
        return UserDto.builder()
            .id(user.getId())
            .firstName(user.getFirstName())
            .lastName(user.getLastName())
            .age(user.getAge())
            .dniStr(user.getDniStr())
            .email(user.getEmail())
            .build();
    }

    public UserDto getUserByEmail(String email) {
        UserEntity user = repository.findByEmail(email);
        return UserDto.builder()
            .id(user.getId())
            .firstName(user.getFirstName())
            .lastName(user.getLastName())
            .age(user.getAge())
            .dniStr(user.getDniStr())
            .email(user.getEmail())
            .build();
    }

    public UserDto addUser(UserDto user) throws Exception{
        UserEntity userEntity = UserEntity.builder()
            .id(user.getId())
            .firstName(user.getFirstName())
            .lastName(user.getLastName())
            .age(user.getAge())
            .dniStr(user.getDniStr())
            .email(user.getEmail())
            .build();
        try{
            repository.save(userEntity);
        } catch (Exception e) {
            throw new RuntimeException("Error al crear usuario");
        }
        return user;
    }

    public UserDto updateUser(UserDto user, long id) throws org.springframework.transaction.TransactionSystemException{
        UserEntity userEntity = repository.findById(id);
        if(userEntity != null) {
            userEntity.setFirstName(user.getFirstName());
            userEntity.setLastName(user.getLastName());
            userEntity.setAge(user.getAge());
            userEntity.setDniStr(user.getDniStr());
            userEntity.setEmail(user.getEmail());
            repository.save(userEntity);
            return user;
        }else {
            return null;
        }
    }

    public void deleteUser(long id) {
        repository.deleteById(id);
	}

    public UserDto findByEmail(String email) {
        UserEntity user = repository.findByEmail(email);
        if(user != null) {
            return UserDto.builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .age(user.getAge())
                .dniStr(user.getDniStr())
                .build();
        }else {
            return null;
        }
    }

    public long getIdByEmail(String email) {
        UserEntity user = repository.findByEmail(email);
        if(user != null) {
            return user.getId();
        }else {
            return -1;
        }
    }

    public void deleteById(long id) {
        repository.deleteById(id);
    }

    public UserDto findById(long id) {
        UserEntity user = repository.findById(id);
        if(user != null) {
            return UserDto.builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .age(user.getAge())
                .dniStr(user.getDniStr())
                .build();
        }else {
            return null;
        }
    }

    public UserEntity getUserEntityById(Long id) {
        Optional<UserEntity> user = repository.findById(id);
        if(user.isPresent()) {
            return user.get();
        }else {
            return null;
        }
    }
}
