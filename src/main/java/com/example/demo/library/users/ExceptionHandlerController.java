package com.example.demo.library.users;

import java.util.HashMap;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
class ExceptionHandlerController {
    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public HashMap<String, Object> NFEException(NotFoundException ex) {
        return new HashMap<String, Object>() {
            {
                put("code", 404);
                put("message", ex.getMessage());
            }
        };
    }

    @ExceptionHandler(org.springframework.transaction.TransactionSystemException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public HashMap<String, Object> MyException(NotFoundException ex) {
        return new HashMap<String, Object>() {
            {
                put("code", 400);
                put("message", "Fallo en el formato de los datos enviados");
            }
        };
    }

}

class NotFoundException extends RuntimeException {
    public NotFoundException(String mensaje) {
        super("Code: 404 - " + mensaje);
    }
}
