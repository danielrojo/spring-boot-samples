package com.example.demo.library.bookvolumes;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.library.books.BookEntity;
import com.example.demo.library.books.BooksRespository;

@Service
public class BookVolumesService {

    @Autowired
    BookVolumesRespository booksRespository;

    @Autowired  
    BooksRespository booksInfoRespository;

    public BookVolumeDto createBook(VolumeRequest book) {
        BookEntity bookInfo = this.booksInfoRespository.findById(book.getBookId());
        System.out.println("bookInfo: " + bookInfo);

        BookVolumeEntity bookEntity = BookVolumeEntity.builder()
                .bookInfo(bookInfo)
                .damageLevel(book.getDamageLevel())
                .isAvailable(book.isAvailable())
                .location(book.getLocation())
                .comments(book.getComments())
                .build();
                
        this.booksRespository.save(bookEntity);
        return this.getDto(bookEntity);
    }

    private BookVolumeDto getDto(BookVolumeEntity bookEntity) {
        return BookVolumeDto.builder()
                .bookInfo(bookEntity.getBookInfo())
                .damageLevel(bookEntity.getDamageLevel())
                .isAvailable(bookEntity.isAvailable())
                .location(bookEntity.getLocation())
                .comments(bookEntity.getComments())
                .build();
    }

    public BookVolumeDto getBookById(long id) {
        BookVolumeEntity be = this.booksRespository.findById(id);
        if (be == null) {
            return null;
        }
        return this.getDto(be);
    }

    public BookVolumeEntity getBookEntityById(long id) {
        return this.booksRespository.findById(id);
    }

    public Iterable<BookVolumeDto> getAllBooks() {
        Iterable<BookVolumeEntity> bookEntities = this.booksRespository.findAll();
        return this.convertBookEntitiesToBookDtos(bookEntities);
    }

    public BookVolumeDto deleteBookById (long id) {
        BookVolumeEntity be = this.booksRespository.findById(id);
        if (be == null) {
            return null;
        }else {
            this.booksRespository.deleteById(id);
            return this.getDto(be);
        }
    }

    public BookVolumeDto modifyBook (BookVolumeDto book, long id){
        BookVolumeEntity be = this.booksRespository.findById(id);
        if (be == null) {
            return null;
        }else {
            be.setBookInfo(book.getBookInfo());

            this.booksRespository.save(be);
            return this.getDto(be);
        }
    }

    private Iterable<BookVolumeDto> convertBookEntitiesToBookDtos(Iterable<BookVolumeEntity> bookEntities) {
        return StreamSupport.stream(bookEntities.spliterator(), false)
                .map(be -> BookVolumeDto.builder()
                        .bookInfo(be.getBookInfo())
                        .damageLevel(be.getDamageLevel())
                        .isAvailable(be.isAvailable())
                        .location(be.getLocation())
                        .comments(be.getComments())
                        .build())
                .collect(Collectors.toList());
    }
    
}
