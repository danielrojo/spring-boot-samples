package com.example.demo.library.bookvolumes;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VolumeRequest {

    @JsonProperty("book_id")
    long bookId;

    @JsonProperty("damage_level")
    private byte damageLevel;

    @JsonProperty("is_available")
    private boolean isAvailable;

    private String location;

    private String comments;

}
