package com.example.demo.library.bookvolumes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class BookVolumesRestController {

    @Autowired
    BookVolumesService booksService;

    @GetMapping("/copies")
    public ResponseEntity<Iterable<BookVolumeDto>> getBooks() {
        return ResponseEntity.ok().body(this.booksService.getAllBooks());
    }

    @PostMapping("/copies")
    public ResponseEntity<BookVolumeDto> createBook(@RequestBody @Validated VolumeRequest book) {
        return ResponseEntity.ok().body(this.booksService.createBook(book));
    }

    @PutMapping("/copies/{id}")
    public ResponseEntity<BookVolumeDto> modifyBook(@RequestBody @Validated BookVolumeDto book, @PathVariable long id) {
        return ResponseEntity.ok().body(this.booksService.modifyBook(book, id));
    }

    @DeleteMapping("/copies/{id}")
    public ResponseEntity<BookVolumeDto> deleteBook(@PathVariable long id) {
        BookVolumeDto book = this.booksService.deleteBookById(id);
        if (book == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().body(book);
        }
    }
    
}
