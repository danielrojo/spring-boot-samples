package com.example.demo.library.bookvolumes;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BookVolumesRespository extends JpaRepository<BookVolumeEntity, Long> {

    BookVolumeEntity findById(long id);

}
