package com.example.demo.library.bookvolumes;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import com.example.demo.library.books.BookEntity;
import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.persistence.Column;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor 
@AllArgsConstructor
@Builder
public class BookVolumeDto {
    
    private BookEntity bookInfo;

    @Range(min = 0, max = 4)
    private byte damageLevel;

    @NonNull
    private String location;

    private boolean isAvailable;

    private String comments;
    
}
