package com.example.demo.library.bookvolumes;

import java.util.List;

import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Value;

import com.example.demo.library.books.BookEntity;
import com.example.demo.library.lends.LendEntity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Entity
@Data
@Builder
@NoArgsConstructor 
@AllArgsConstructor
@Table(name = "volumes")
public class BookVolumeEntity {

    @Id
    @GeneratedValue(strategy = jakarta.persistence.GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "book_id")
    private BookEntity bookInfo;

    @OneToMany(mappedBy = "book")
    private List<LendEntity> lends;

    @Range(min = 0, max = 4)
    private byte damageLevel;

    @Value("true")
    private boolean isAvailable;

    @NonNull
    private String location;

    private String comments;


}
