package com.example.demo;


import static org.assertj.core.api.Assertions.assertThat;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class SmokeTest {

	@Autowired
	private GreetingController controller;

	@Autowired
	private GreetingController greetingController;

	@BeforeAll
	public static void setUpBeforeClass() throws Exception {

	}

	@AfterAll
	public static void tearDownAfterClass() throws Exception {

	}

	@BeforeEach
	public void setUp() throws Exception {

	}

	@AfterEach
	public void tearDown() throws Exception {

	}

	@Test
	@Order(2)
	public void contextLoads() throws Exception {
		assertThat(controller).isNotNull();
		assertThat(greetingController).isNotNull();
	}

	@Test
	@Order(1)
	public void greetingShouldReturnDefaultMessage() throws Exception {
		assertThat(this.greetingController.greeting()).contains("Hello, World");
	}


}
