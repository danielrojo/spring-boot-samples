package com.example.demo;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class CalculatorTest {

	@Autowired
	private CalculatorController calculatorController;

	@Autowired
	private CalculatorService calculatorService;


	@BeforeEach
	public void setUp() throws Exception {

	}

	@AfterEach
	public void tearDown() throws Exception {

	}

	@Test
	public void contextLoads() throws Exception {
		assertThat(calculatorController).isNotNull();
		assertThat(calculatorService).isNotNull();
	}

	@Test
	public void calculatorShouldReturnDefaultMessage() throws Exception {
		assertThat(this.calculatorService.calculate(1, 1, "+")).isEqualTo(2);
	}

	//check if the calculator can divide by zero returns infinity
	@Test
	public void calculatorShouldReturnDefaultMessage2() throws Exception {
		assertThat(this.calculatorService.calculate(1, 0, "/")).isEqualTo(Double.POSITIVE_INFINITY);
	}

	//check that if the operator is not supported, the method throws an exception
	@Test
	public void calculatorShouldReturnDefaultMessage3() throws Exception {
		assertThrows(IllegalArgumentException.class, () -> {
			this.calculatorService.calculate(1, 1, "a");
		});
		
	}

	//check what happens when the calculator is given null operator
	@Test
	public void calculatorShouldReturnDefaultMessage4() throws Exception {
		assertThrows(NullPointerException.class, () -> {
			this.calculatorService.calculate(1, 1, null);
		});
	}

	//check what happens when the calculator is given 0/0
	@Test
	public void calculatorShouldReturnDefaultMessage5() throws Exception {
		assertThrows(IllegalArgumentException.class, () -> {
			this.calculatorService.calculate(0, 0, "/");
		});
	}
	

}
