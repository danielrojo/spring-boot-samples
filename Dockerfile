FROM openjdk:17-jdk-slim-buster AS builder
WORKDIR /app
COPY . .
RUN ./mvnw clean test package -DskipTests

FROM openjdk:17-alpine
WORKDIR /app
COPY --from=builder /app/target/*.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "app.jar"]


